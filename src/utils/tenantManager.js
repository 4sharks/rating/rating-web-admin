export const tenant = () => JSON.parse( localStorage.getItem('tenant'));
export const getCompanyById = (id) => tenant()?.companies?.filter(company => company._id === id)[0]

