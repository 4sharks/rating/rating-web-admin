import React from 'react'

const TitlePage = ({children}) => {
    return (
        <div className='px-2'>
            <h3>{children}</h3>
        </div>
    )
}

export default TitlePage