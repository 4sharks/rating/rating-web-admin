import React from 'react'

const MwInputGroup = (props) => {
    const {label,inputOneId,inputOneValue,inputOneOnChange,inputTwoId,inputTwoValue,inputTwoOnChange} = props
    return (
        <>
            <label className='text-sm text-slate-500' htmlFor="Customer">{label}</label>
            <div className="flex  ">
                <div className="flex flex-col w-40">
                    <input 
                        type="text" 
                        className='border-t text-slate-600 border-b border-l  rounded-l-lg p-1 text-center' 
                        id={inputOneId} 
                        name={inputOneId} 
                        value={inputOneValue} 
                        onChange={inputOneOnChange} />
                </div>
                <div className="flex flex-1 flex-col">
                    <input 
                        type="text" 
                        className='border text-slate-600 rounded-r-lg p-1 text-center' 
                        id={inputTwoId} 
                        name={inputTwoId} 
                        value={inputTwoValue} 
                        onChange={inputTwoOnChange}/>
                </div>
            </div>
        </>
    )
}

export default MwInputGroup