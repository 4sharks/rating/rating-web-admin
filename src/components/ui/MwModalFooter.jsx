import React from 'react'

const MwModalFooter = (props) => {
    return (
        <div className='flex absolute sticky bottom-0 gap-2 bg-slate-200 p-2 rounded-b mt-2 border-t border-slate-400 '>
            {props.children}
        </div>
    )
}

export default MwModalFooter