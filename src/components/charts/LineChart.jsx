import {
    Chart as ChartJS,
    LineElement,
    PointElement,
    CategoryScale,
    LinearScale,
    Tooltip,
    Legend
} from 'chart.js';
import {Chart, Line} from 'react-chartjs-2'
import { MwSpinnerButton } from '../ui';

ChartJS.register(
    LineElement,
    PointElement,
    CategoryScale,
    LinearScale,
    Tooltip,
    Legend
)

const LineChart = ({label,datasetData,loading,className}) => {
    const data = {
        labels: ['يناير', 'فبراير', 'مارس', 'ابريل','مايو', 'يونيو', 'يوليو', 'اغسطس', 'سمبتمبر', 'اكتوبر','نوفمبر', 'ديسمبر'],
        datasets:[
            {
                label:label ,
                data:datasetData,
                borderColor: '#906d04',
                backgroundColor: '#f5b800',
                tension:0.4,

            }
        ]
    }
    const options = {
        plugins: {
            // defaultFontFamily:'"Tajawal", sans-serif',
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    font: {
                        size: 14,
                        family: '"Tajawal", sans-serif',
                    }
                }
            }
        }
    }

    return (
        <div className='rounded-lg bg-slate-50 p-2 flex-1 border-2 border-slate-50 flex items-center justify-center '>
            { !loading ? <Line className={`w-full ${className}`}
            data={data}
            options={options}
            >
            </Line> :  <MwSpinnerButton withLabel={false} isFullCenter={true} />}
        </div>
            
    )
}

export default LineChart