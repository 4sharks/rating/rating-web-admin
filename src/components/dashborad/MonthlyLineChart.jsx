import React from 'react'
import { MwButton, MwSpinnerButton, NoDataFound } from '../ui'
import LineChart from '../charts/LineChart'
import { Link } from 'react-router-dom'

const MonthlyLineChart = ({
    tenantUsername,
    servicesLoading,
    servicesResult,
    serviceSelected,
    ratingByService,
    ratingByServiceLoading
}) => {
  return (
    <>
            {
                servicesLoading ? 
                    <MwSpinnerButton isFullCenter={true} withLabel={false}/>
                    : servicesResult.data.length > 0 ?
                        <div className={`flex-1  ${servicesResult.data.length === 1 ? 'flex-col' : 'flex-row gap-3' }`}>
                            { 
                                serviceSelected && 
                                    <LineChart 
                                        label={serviceSelected.serviceName} 
                                        datasetData = {ratingByService} 
                                        loading={ratingByServiceLoading} 
                                        /> 
                            }
                        </div> :
                        <NoDataFound msg={`لا يوجد خدمات تقييم   `}>
                            <div className='flex gap-3  justify-center items-center'>
                                <Link to={`/${tenantUsername}/rating-services`}>
                                    <MwButton type='saveBtn'>أنشاء خدمة جديدة</MwButton> 
                                </Link>
                            </div>
                        </NoDataFound>
            } 
    </>
  )
}

export default MonthlyLineChart